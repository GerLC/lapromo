class ReservationCommentController < ApplicationController
    include ApplicationHelper 
    
    before_action :authenticate_user!, :check_for_diner_user, :retrieve_reservation, :reservation_can_be_commented?
    skip_before_action :restaurant_without_data, :redirect_restaurant

    def new
        @restaurant = @reservation.restaurant
    end

    def create
        comment = @reservation.create_comment(
            kitchen_rating: params[:kitchen_score],
            service_rating: params[:service_score],
            ambient_rating: params[:ambient_score],
            description: params[:comment],
            attached_photos: params[:photos],
            invite_email: params[:invite_email]
        )

        if (comment.valid?)
            if comment.invite_email && ! comment.invite_email.empty? 
                InviteFriendToRestaurantMailer.with(reservation: @reservation, friend_email: comment.invite_email).send_invitation.deliver_now
            end

            flash[:notice] = "Tu comentario al restaurant #{@reservation.restaurant.name} ha sido procesada satisfactoriamente"
            redirect_to '/home/index'
        else
            flash[:error] = comment.errors.full_messages
            redirect_to "/reservations/#{@reservation.id}/comment"
        end
    end

    private
        def retrieve_reservation
            @reservation = Reservation.find_by_id(params[:id])
        end

        def reservation_can_be_commented?
            if (! @reservation || !@reservation.used? || @reservation.comment)
                render :file => "public/401.html", :status => :unauthorized, :layout => false
            end
        end
end
