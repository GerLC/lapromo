class Administrador::NoShowsController < ApplicationController
    before_action :authenticate_admin!
    before_action :set_unblock_diner, only: [:blocked_diners,:unblock_diners]
    before_action :set_diner, only: [:warning_diners,:should_diner_block,:block_diner, :warn_diner]
    before_action :set_diner_gift, only: [:gift_diner,:giving_yumis]
    layout 'administrador'
    ## metodo para obtener comensales con reservas no efectuadas para advertencia
    def warning_diners
        @contador= 0
        @diners.each do |din|
            if din.reservations.to_a.length == 2 
                @contador +=  1
            end
        end
    end
    ## metodo para obtener comensales con reservas no efectuadas para concurrentes
    def should_diner_block
        @contador = 0
        @diners.each do |din|
            if din.reservations.to_a.length > 2 && din.user.approved == true
                @contador +=  1
            end
        end  
    end
    #metodo que bloquea comensales concurrentes
    def block_diner
        user = Diner.find(params[:id]).user
        user.approved = false
        puts user.approved
        if user.save
            flash[:success]= "Se ha bloqueado correctamente"
        else
            flash[:warning] = "No se ha podido bloquear"
        end
        puts user.approved

        render :should_diner_block
    end
    ## metodo para obtener comensales bloqueados y renderizar la vista de bloqueados
    def blocked_diners
    end
    ## metodo para desbloquear usuarios
    def unblock_diners
        user = Diner.find(params[:id]).user
        user.approved = true
        if user.save
            flash[:success] = "Se ha desbloqueado correctamente"
        else
            flash[:warning] = "Error al desbloquear"
        end
        render :blocked_diners
    end
    ## metodo para enviar correo de advertencia a comensal
    def warn_diner
        @user = Diner.find(params[:id]).user
        NoShowsMailer.with(user: @user).warning_diner_email.deliver_now
        flash[:success] = "Correo enviado satisfactoriamente a #{@user.email}"
        render :warning_diners
    end
    ## Metodo para obtener comensales para premiarlos
    def gift_diner

    end
    ##METODO PARA OTORGAR PUNTOS YUMIS AL COMENSAL SELECCIONADO
    def giving_yumis
        diner = Diner.find(params[:id])
        hist_yum = HistYumi.new(type_hist_yumis: "Reserva efectuada",quantity:300,
                    diner_id: diner.id)
        if hist_yum.save
            flash[:success] = "Se otorgaron 300 yumis al comensal #{diner.name}."
        else
            flash[:warning] = "No se pudo otorgar yumis al comensal."
        end
        render :gift_diner
    end
    ### metodos para reusabilidad de codigo
    private 
    ### metodo para setear comensales bloqueados
    def set_unblock_diner
        @diners = Diner.joins(:user).where("users.approved = 'false'")
    end
    ### metodo para setear comensales con reservas no efectuadas
    def set_diner
        @diners = Diner.includes(:reservations).where.not(reservations: {id: nil})
        .where("lower(status) = 'no efectuada'")
    end
    ### metodo para setear comensales con reservas efectuadas
    def set_diner_gift
        @diners = Diner.includes(:reservations).where.not(reservations: {id: nil})
        .where("lower(status) = 'efectuada'")
    end
    
    
end
