class Api::V1::ApiInvitationController < ApplicationController
  skip_before_action :verify_authenticity_token
  prepend_before_action :require_no_authentication, :only => [:create]

  def api_invitation
      @reserva = Reservation.find(params[:id])
      ApiInvitationMailer.with(promo:@reserva.promo, diner: @reserva.diner,restaurant: @reserva.restaurant, reservation: @reserva, friend_mail:params[:friend_mail]).api_new_Invitation_email.deliver_now
      return render json: @reserva, :status => :ok
  end

#Resumen de reserva
def api_reservation
  @diner = Diner.find_by(user_id:params[:user_id])

  @reserva = Reservation.new(
    reservation_date: params[:reservation_date],
    people_num: params[:people_num],
    status:'Pendiente',
    promo_id: params[:promo_id],
    hist_yumi_id: 1,
    restaurant_id: params[:restaurant_id],
    diner_id: @diner.id
    )
    if @reserva.save
        @link = 'http://la-promo.herokuapp.com/api/v1/api_reservation/'+(@reserva.id).to_s+'/1478965'
        ApiInvitationMailer.with(promo:@reserva.promo, diner: @reserva.diner,restaurant: @reserva.restaurant, reservation: @reserva, user: @diner.user,link:@link).api_reserve_resume.deliver_now
      return render json: @reserva, :status => :ok
    else
      failure
    end
end



  #Envio de correos para cancelar una reserva
  def api_reserve_cancel
     Reservation.update(params[:id], :status => 'Cancelada')
     @reserva = Reservation.find(params[:id])
     ApiInvitationMailer.with(promo:@reserva.promo, diner: @reserva.diner,restaurant: @reserva.restaurant, reservation: @reserva, user: @reserva.diner.user).api_reserve_cancel.deliver_now
     return render json: 'Reserva cancelada', :status => :ok
  end

  #Envio de correos para cancelar una reserva
  def api_reserve_invitation_yumies

     @diner = Diner.find_by(user_id:params[:user_id])
     Diner.update(@diner.id, :total_yumis => @diner.total_yumis+10)
     ApiInvitationMailer.with(user:@diner.user).api_reserve_invitation_yumies.deliver_now
     return redirect_to "http://la-promo.herokuapp.com/"
  end


  #Metodo de mensaje de error cuando no se logra autenticar con el login
  def failure
    return render json: { success: false, errors: 'Datos invalidos'}, :status => :unauthorized
  end
end
