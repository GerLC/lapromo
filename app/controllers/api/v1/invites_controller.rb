class Api::V1::InvitesController < ApplicationController
    skip_before_action :verify_authenticity_token
    def invitar
        if (!params[:correo].nil?)
            @user = User.where(user_name: params[:username]).take
            @diner = Diner.find(@user.diner.id)
            params[:correo].each {
                |x|
                ApiInviteMailer.with(diner:@diner,correo:x).invite_mailer.deliver_now
            }
        end
    end
end