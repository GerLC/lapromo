class NoShowsMailer < ApplicationMailer
    def warning_diner_email
        @diner = params[:user]
        mail(to: "#{@diner.email}", subject: 'Advertencia de bloqueo'  )
    end 
end
