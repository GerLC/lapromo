class ApiInvitationMailer < ApplicationMailer

  def api_new_Invitation_email
    @diner = params[:diner]
    @restaurant = params[:restaurant]
    @reserva = params[:reservation]
    @promo = params[:promo]
    @mensaje = params[:mensaje]
    @friend_mail = params[:friend_mail]
    mail(to: "#{@friend_mail}", subject: "#{@diner.name} #{@diner.last_name} te ha invitado a la promo")
  end


  def api_reserve_resume
    @diner = params[:diner]
    @reserva = params[:reservation]
    @user = params[:user]
    @restaurant = params[:restaurant]
    @promo = params[:promo]
    @link = params[:link]
    mail(to: "#{@user.email}", subject: "Resumen de Reserva nro: #{@reserva.id}")
  end

  def api_reserve_cancel
    @diner = params[:diner]
    @reserva = params[:reservation]
    @user = params[:user]
    @restaurant = params[:restaurant]
    @promo = params[:promo]
    mail(to: "#{@user.email}", subject: "Cancelacion de Reserva nro: #{@reserva.id}")
  end

  def api_reserve_invitation_yumies
    @user = params[:user]
    mail(to: "#{@user.email}", subject: "Tenemos algo para ti!")
  end





end
