class ApiInviteMailer < ApplicationMailer
    def invite_mailer
        @from = 'lapromoucab2020@gmail.com'
        @mail = params[:correo]
        @diner = params[:diner]
        @url = 'http://la-promo.herokuapp.com/users/sign_up?referido='+@diner.user.user_name
        mail(from: @from, to: @mail, subject: @diner.name + ' Te invita a registrarte en la pagina de la promo')
    end 
end