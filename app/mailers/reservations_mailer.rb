class ReservationsMailer < ApplicationMailer

	def cancel_reservation_email
		@diner = params[:diner]
	    @reserva = params[:reservation]

	    mail(to: @diner.user.email, subject: "La reserva en " + @reserva.restaurant.name + " se ha cancelado!")
	end

	def new_reservation_email 
		@reservation = params[:reservation]
		@restaurant = params[:restaurant]
		@user = params[:user]
		@diner = params[:diner]
		@promo = params[:promo]
		mail(to: @user.email, subject: "Datos de la Reserva")
	end

end
