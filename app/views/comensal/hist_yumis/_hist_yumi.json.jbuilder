json.extract! hist_yumi, :id, :created_at, :updated_at
json.url hist_yumi_url(hist_yumi, format: :json)
