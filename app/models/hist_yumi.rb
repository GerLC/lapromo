class HistYumi < ApplicationRecord
  belongs_to :diner
  has_one :invitation
  has_one :reservation
  after_save :set_total_yumis
  validates :quantity, presence: {message: "Debe colocar cantidad de yumis"}
  validates :type_hist_yumis, presence: {message: "Debe colocar el tipo de historia yumi"}
  ## Scope para obtener el historial de yumis total
  scope :yumis_personal, -> (comensal) { HistYumi.joins(:diner)
    .where("diners.id = #{comensal}").order("created_at DESC")}
  ## Scope para obtener el historial de yumis por invitar amigos
  scope :yumis_amigos, -> (comensal) { HistYumi.joins(:diner)
    .where("diners.id = #{comensal}").where("lower(type_hist_yumis) = 'invitar amigo'")
    .order("created_at DESC")}
  ## Scope para obtener el historial de yumis por reservas efectuadas
  scope :yumis_reservas_efectuadas, -> (comensal) { HistYumi.joins(:diner)
    .where("diners.id = #{comensal}").where("lower(type_hist_yumis) = 'reserva efectuada'")
    .order("created_at DESC")}
  ## Scope para obtener el historial de yumis por reservas no efectuadas
  scope :yumis_reservas_noefectuadas, -> (comensal) { HistYumi.joins(:diner)
    .where("diners.id = #{comensal}").where("lower(type_hist_yumis) = 'reserva no efectuada'")
    .order("created_at DESC")}
  ## Scope para obtener el historial de yumis por reservas canceladas
  scope :yumis_reservas_canceladas, -> (comensal) { HistYumi.joins(:diner)
    .where("diners.id = #{comensal}").where("lower(type_hist_yumis) = 'reserva cancelada'")
    .order("created_at DESC")}
  
    #### METODO PARA GUARDAR TOTAL DE YUMIS
  def set_total_yumis
    @total_yumis = 0
    @diner = Diner.where("id = #{self.diner_id}")
    @diner.each do |diner|
      @hist_yumis = HistYumi.where("diner_id = #{diner.id}").all
      @hist_yumis.each do |hist|
        @total_yumis += hist.quantity
      end
      diner.total_yumis = @total_yumis
      diner.save
    end
  end
  


end
