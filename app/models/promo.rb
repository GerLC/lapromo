class Promo < ApplicationRecord
	# Definiendo relaciones para promo
	belongs_to :restaurant
	has_many :promo_plates, :dependent => :delete_all
	has_many :plates, through: :promo_plates
	has_many :promo_calendars, :dependent => :delete_all
	has_many :calendars, through: :promo_calendars
	has_many :reservations, :dependent => :delete_all
	accepts_nested_attributes_for :calendars
	accepts_nested_attributes_for :promo_calendars


	scope :promos_of_restaurant, ->(id) {where("restaurant_id =" + id).distinct('promos.id').select('promos.*')}
	scope :dias_promo, ->(id) {joins("JOIN promo_calendars ON promo_calendars.promo_id = promos.id").joins("JOIN calendars ON calendars.id = promo_calendars.calendar_id AND promos.restaurant_id = " + id).select('calendars.final_date,promos.name,promos.id,promos.promo_type,promos.yumis')}
	scope :restaurants2, -> (id) {where("restaurant_id = ?",id).pluck('description')}
	has_many :reservations, :dependent => :delete_all


	scope :restaurants, -> (id) {where("restaurant_id = ?",id).pluck('promo_type')}


	# TDD para el modelo promo
	validates :name, :presence => {:message => "Falta el nombre de la promoción"}
	validates :promo_type, :presence => {:message => "Falta el tipo de promoción"}
	validates :description, :presence => {:message => "Falta la descripción"}
	validates :name, length: {minimum: 5, :message => "El nombre de la promoción debe tener al menos 5 caracteres"}
	after_create :send_new_promo_emails
	after_update :check_if_emails_must_be_sent
	def plates= new_array
		old_array = self.plates
		@plates_changed = self.has_many_changed?(old_array, new_array)
		super new_array
	end
	def calendars= new_array
		old_array = self.calendars
		@calendars_changed = self.has_many_changed?(old_array, new_array)
		super new_array
	end
	def send_new_promo_emails
		self.restaurant.favorites.all.each do |favorite|
			if favorite.diner.favorite_notif
				PromoMailer.with(diner: favorite.diner, promo: self).new_promo_mail.deliver_now
			end
		end
	end
	def send_updated_promo_emails
		self.restaurant.favorites.all.each do |favorite|
			if favorite.diner.favorite_notif
				PromoMailer.with(diner: favorite.diner, promo: self).updated_promo_mail.deliver_now
			end
		end
	end
	def check_if_emails_must_be_sent
		name_changed = self.name_previously_changed?
		promo_type_changed = self.promo_type_previously_changed?
		description_changed = self.description_previously_changed?
		yumis_changed = self.yumis_previously_changed?
		if name_changed || promo_type_changed || description_changed || yumis_changed || @plates_changed || @calendars_changed
			self.send_updated_promo_emails
		end
	end
	def has_many_changed?(old_plates, new_plates)
		a = old_plates.map(&:id)
		b = new_plates.map(&:id)
		c = a - b | b - a
		c.count > 0
	end

	def self.search(term, page)
		if term
		  where('nombre LIKE ?', "%#{term}%").paginate(page: page, per_page: 5).order('id DESC')
		else
		  paginate(page: page, per_page: 5).order('id DESC')
		end
	  end




	scope :promo_activa, ->(id, ini, fin) {joins("JOIN promo_calendars ON promo_calendars.promo_id = promos.id").joins("JOIN calendars ON calendars.id = promo_calendars.calendar_id AND promos.restaurant_id = " + id).where("calendars.final_date >= '"+ini+"' AND calendars.final_date <= '"+fin+"'").select('promos.name,promos.promo_type').distinct}



#Scopes grupo_4
scope :list_of_promo_detail, ->{find_by_sql('
	SELECT
pr.id,
pr.restaurant_id,
re.name name_restaurant,
pr.name  name_promo,
pr.promo_type,
pr.description descrip_promo,
pr.yumis,
ca.created_at as inital_date,
ca.final_date
FROM  promos AS PR
INNER JOIN restaurants     AS RE ON RE.id = pr.restaurant_id
INNER JOIN promo_calendars AS PC ON PC.promo_id = PR.id
INNER JOIN calendars AS CA ON CA.id = PC.calendar_id')}

scope :promo_dates_list, ->(id,rest_id) {find_by_sql('
	SELECT
	pr.id,
	pr.restaurant_id,
	re.name name_restaurant,
	pr.name  name_promo,
	pr.promo_type,
	pr.description descrip_promo,
	pr.yumis,
	ca.final_date as promo_date,
	ca.final_date
	FROM  promos AS PR
	INNER JOIN restaurants     AS RE ON RE.id = pr.restaurant_id
	INNER JOIN promo_calendars AS PC ON PC.promo_id = PR.id
	INNER JOIN calendars AS CA ON CA.id = PC.calendar_id
	WHERE pr.promo_type = ' +'\'' + id + '\''+'
	AND RE.id = '+rest_id )}


	scope :promo_dates_list_mb, ->(id,rest_id) {find_by_sql('
		SELECT
		pr.id,
		pr.restaurant_id,
		CONCAT('+'\'Restaurant: '+'\', re.name,'+' \' Promo: '+'\' ,pr.name,'+'\' Descripcion:'+'\', pr.description) as text,
		pr.yumis,
		ca.created_at as start,
		ca.created_at as end,
		(select  (array['+'\'#ca4747'+'\', '+'\'#f67944'+'\', '+'\'#f67944'+'\','+'\'#00aabb'+'\'])[floor(random() * 4 + 1)]) as color
		FROM  promos AS PR
		INNER JOIN restaurants     AS RE ON RE.id = pr.restaurant_id
		INNER JOIN promo_calendars AS PC ON PC.promo_id = PR.id
		INNER JOIN calendars AS CA ON CA.id = PC.calendar_id
		WHERE pr.promo_type = ' +'\'' + id + '\''+'
		AND RE.id = '+rest_id )}


	scope :promo_type, ->(id){find_by_sql('select distinct(promo_type) as name,restaurants.name as restaurant_name, restaurants.id as id from promos
inner join restaurants on restaurants.id= promos.restaurant_id
where restaurant_id ='+id)}

scope :promos_list_of_restaurant, ->(id) {where("restaurant_id =" + id).distinct('promos.id').select('promos.*')}
#FIn Scopes grupo 4

end
