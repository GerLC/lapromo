# Preview all emails at http://localhost:3000/rails/mailers/comment_mailer
class CommentMailerPreview < ActionMailer::Preview

    include FactoryBot::Syntax::Methods

    def new_recommendation_mail
        @comment = Comment.find(3)
        @reservation = @comment.reservation
        @promo = @reservation.promo
        @diner = @reservation.diner
        @mail = 'ferdy.rod23@gmail.com'



        CommentMailer.with(correo: @mail, diner: @diner, promo: @promo, comment: @comment).new_recommendation_mail
    end

end
