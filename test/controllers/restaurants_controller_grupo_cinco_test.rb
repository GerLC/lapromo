require 'test_helper'

class RestaurantsControllerGrupoCincoTest < ActionController::TestCase
  
  include Devise::Test::ControllerHelpers

  def setup
    @controller=RestaurantsController.new
  end

  #test de prueba del warning para busqueda por estado
  test "flash warning, no hay restaurantes en el estado" do
    get :buscar_x_estado, params:{place_id: 503}
    assert_equal 'No hay restaurantes registrados en ese estado', flash[:warning]
    assert_not_nil assigns(:restaurants)
  end

  #test de prueba del warning para busqueda por tipo de comida
  test "flash warning, no hay restaurantes con ese tipo de comida" do 
    get :buscar_x_tipo_comida, params:{name: "Comida tipica"}
    assert_equal 'No hay restaurantes registrados con ese tipo de comida', flash[:warning]
    assert_not_nil assigns(:restaurants)
  end

  #test que desmuestra que la busqueda fue satisfactoria de restaurantes por estado
  test "ubica path buscar x estado" do 
    get :buscar_x_estado, params:{place_id: 1}
    assert_response :found
    assert_not_nil assigns(:restaurants)
  end

  #test que demuestra que la busqueda fue satisfactoria de restaurantes por tipo de comida
  test "ubica path buscar x tipo de comida" do
    get :buscar_x_tipo_comida, params:{name: "Comida italiana"}
    assert_response :found
    assert_not_nil assigns(:restaurants)
  end

  #test de prueba del warning para busqueda por tipo de promocion
  test "flash warning, no hay restaurantes con ese tipo de promocion" do 
    get :buscar_x_tipo_promo, params:{name: "3x1"}
    assert_equal 'No hay restaurantes registrados con ese tipo de promocion', flash[:warning]
    assert_not_nil assigns(:restaurants)
  end

  
   #test que demuestra que la busqueda fue satisfactoria de restaurantes por tipo de promocion
   test "ubica path buscar x tipo de promocion" do
    get :buscar_x_tipo_promo, params:{name: "2x1"}
    assert_response :found
    assert_not_nil assigns(:restaurants)
  end
end
