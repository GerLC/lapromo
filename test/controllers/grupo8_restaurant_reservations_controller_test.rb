require 'test_helper'

class Grupo8RestaurantReservationsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers
  include ActionMailer::TestHelper
  include ActionMailer::TestCase::Behavior

  setup do
    @controller = Restaurante::ReservationsController.new
  end

  test "logged_in_restaurants_can_access_their_reservations_page" do
    restaurant = create(:restaurant)

    r1 = create(:reservation, restaurant: restaurant, reservation_date: '2020-01-02')
    r2 = create(:reservation, restaurant: restaurant, reservation_date: '2020-01-01')
    r3 = create(:reservation,  restaurant: restaurant, reservation_date: '2020-01-03')
  
    sign_in restaurant.user

    get :index

    assert_response :success

    # check that reservations that go to the view are ordered by date desd
    reservations_for_view = @controller.instance_variable_get(:@reservations)
    
    assert_equal r3.id, reservations_for_view[0].id
    assert_equal r1.id, reservations_for_view[1].id
    assert_equal r2.id, reservations_for_view[2].id
  end

  test "only_logged_in_users_can_access_reservations_page" do
    get :index

    assert_redirected_to :new_user_session
  end

  test "only_restaurants_can_access_their_reservations_page" do
    diner = create(:diner)

    sign_in diner.user

    get :index

    assert_response 401
  end

  test "reservation_status_can_be_changed" do
    restaurant = create(:restaurant)
    reservation = create(:reservation, restaurant: restaurant, status: Reservation::PENDING)

    sign_in restaurant.user
    
    post :update_status, params: { id: reservation.id, new_status: Reservation::USED }

    assert_equal Reservation::USED, reservation.reload.status
    assert_equal 'Se ha cambiado satisfactoriamente el status de la reserva', flash[:notice]

    assert_response 302
  end 

  test "new_reservation_status_must_be_valid" do
    restaurant = create(:restaurant)
    reservation = create(:reservation, restaurant: restaurant, status: Reservation::PENDING)

    sign_in restaurant.user
    
    post :update_status, params: { id: reservation.id, new_status: 'non-valid-status' }

    assert_equal Reservation::PENDING, reservation.reload.status
    assert_equal 'El status al que se intento cambiar la reserva es invalido', flash[:notice]

    assert_response 302
  end

  test "when_changing_status_to_used_an_invite_for_comment_restaurant_must_be_send" do
    restaurant = create(:restaurant)
    reservation = create(:reservation, restaurant: restaurant, status: Reservation::PENDING)

    sign_in restaurant.user
    
    post :update_status, params: { id: reservation.id, new_status: Reservation::USED }
    
    assert_emails 1
  end

end
