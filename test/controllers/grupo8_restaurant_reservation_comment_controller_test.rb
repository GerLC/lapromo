require 'test_helper'

class Grupo8RestaurantReservationCommentControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  include ActionMailer::TestHelper
  include ActionMailer::TestCase::Behavior

  setup do
    @controller = ReservationCommentController.new
  end   

  def get_url(reservation)
    "/reservations/#{reservation.id}/comment"
  end

  # These tests are general checks for both the "new" and "create" endpoints

  test "authorized_users_can_see_the_comment_form" do
    diner = create(:diner, favorite_notif: false)
    reservation = create(:reservation, diner: diner, status: Reservation::USED)

    sign_in diner.user

    get get_url(reservation), params: {id: reservation.id}

    assert_response :success
  end

  test "users_have_to_be_authenticated" do
    diner = create(:diner, favorite_notif: false)
    reservation = create(:reservation)

    get get_url(reservation), params: {id: reservation.id}

    assert_redirected_to :new_user_session
  end

  test "only_diner_users_can_access" do
    reservation = create(:reservation)

    restaurant = create(:restaurant)

    sign_in restaurant.user

    get get_url(reservation), params: {id: reservation.id}

    assert_response 401
  end

  test "provided_reservation_must_be_valid" do
    diner = create(:diner, favorite_notif: false)

    sign_in diner.user

    get "/reservations/-1/comment", params: {id: "-1"}

    assert_response 401
  end

  test "only_reservations_that_have_been_marked_as_used_can_have_a_comment" do
    diner = create(:diner, favorite_notif: false)
    reservation = create(:reservation, diner: diner, status: Reservation::PENDING)

    sign_in diner.user

    get get_url(reservation), params: {id: reservation.id}

    assert_response 401
  end

  test "only_reservation_that_do_not_already_have_a_comment_are_allowed" do
    diner = create(:diner, favorite_notif: false)
    reservation = create(:reservation, diner: diner, status: Reservation::USED)
    comment = create(:comment, reservation: reservation)
    sign_in diner.user

    get get_url(reservation), params: {id: reservation.id}

    assert_response 401
  end

  test "authorized_users_can_post_a_comment" do
    diner = create(:diner, favorite_notif: false)
    reservation = create(:reservation, diner: diner, status: Reservation::USED)

    assert_nil reservation.comment

    sign_in diner.user

    photos = [
        fixture_file_upload('files/comment_test_image_1.jpg','image/jpeg'), 
        fixture_file_upload('files/comment_test_image_2.jpg','image/jpeg'),
        fixture_file_upload('files/comment_test_image_3.jpg','image/jpeg'),
    ]

    Services::ImageUploadService.expects(:upload).with(instance_of(String)).returns('path-to-photo-1')
    Services::ImageUploadService.expects(:upload).with(instance_of(String)).returns('path-to-photo-2')
    Services::ImageUploadService.expects(:upload).with(instance_of(String)).returns('path-to-photo-3')

    post get_url(reservation), params: {
      id: reservation.id,
      kitchen_score: 5,
      service_score: 4,
      ambient_score: 5,
      photos: photos,
      comment: "Excelente lugar, lo recomiendo",
      invite_email: ""
    }, headers: {'content-type': 'multipart/form-data' }

    assert_redirected_to '/home/index'
    assert_equal "Tu comentario al restaurant #{reservation.restaurant.name} ha sido procesada satisfactoriamente", flash[:notice]

    comment = reservation.reload.comment
    
    assert_not_nil comment
    assert_equal 5, comment.kitchen_rating
    assert_equal 4, comment.service_rating
    assert_equal 5, comment.ambient_rating
    assert_equal "Excelente lugar, lo recomiendo", comment.description
    
    photos = comment.photos

    assert_equal 3, photos.count
    assert_equal 'path-to-photo-3', photos[0].path
    assert_equal 'path-to-photo-2', photos[1].path
    assert_equal 'path-to-photo-1', photos[2].path
  end

  test "users_can_invite_a_fried_to_enjoy_the_rest_promo_when_leaving_a_comment" do
    diner = create(:diner, favorite_notif: false)
    reservation = create(:reservation, diner: diner, status: Reservation::USED)

    assert_nil reservation.comment

    sign_in diner.user

    photos = [nil]

    post get_url(reservation), params: {
      id: reservation.id,
      kitchen_score: 5,
      service_score: 4,
      ambient_score: 5,
      photos: photos,
      comment: "Excelente lugar, lo recomiendo",
      invite_email: "luisvdbk312@gmail.com"
    }, headers: {'content-type': 'multipart/form-data' }

    comment = reservation.reload.comment
    
    assert_not_nil comment
    
    assert_emails 1
  end
  
end
