require 'test_helper'
class RestaurantsgrupocuatroControllerTest < ActionController::TestCase
include Devise::Test::ControllerHelpers
    def setup
      @controller = Api::V1::RestaurantsController.new
    end

    test "Status code error en Fltro de nombre" do
        get 'restaurant_name', params:{:q => "1a1"} # 1
        assert_response(404)
    end

    test "Mensaje de error en Fltro de nombre" do
        get 'restaurant_name', params:{:q => "1a1"} # 1
        resp = JSON.parse(response.body)
        assert_equal 'No se encontraron restaurantes',resp['error']
    end


    test "Mensaje de error en Fltro dinamico" do
        get 'restaurant_search', params:{:id => "1",:kt => "0"} # 1
        resp = JSON.parse(response.body)
        assert_equal 'No hay resultados, intente con otro filtro',resp['error']
    end

    test "Status code error en Fltro dinamico" do
        get 'restaurant_search', params:{:id => "1",:kt => "0"} # 1
        assert_response(404)
    end

    test "Status code OK en Fltro dinamico" do
        get 'restaurant_search', params:{:id => "1"} # 1
        assert_response(200)
    end

    test "Status code OK en Fltro dinamico 2 parametros" do
        get 'restaurant_search', params:{:id => "1",:kt => "501"} # 1
        assert_response(200)
    end

    test "Informacion en Fltro dinamico 2 parametros" do
        get 'restaurant_search', params:{:id => "1",:kt => "501"} # 1 y tipo de cocina
        resp = JSON.parse(response.body)
        assert_equal 501,resp.first['kt'].find{|det| det['id']==501}['id']
    end

    test "Informacion en lista de restaurant" do
        get 'restaurant_list', params:{:id => "1"} # 1 y tipo de cocina
        assert_response(200)
    end

    test "Status code ok de lista de restaurant" do
      get 'restaurant_list',params:{:id => "1"} # 1
      resp = JSON.parse(response.body)
      assert_not_empty resp
    end


end
