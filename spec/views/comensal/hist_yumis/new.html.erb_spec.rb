require 'rails_helper'

RSpec.describe "comensal/hist_yumis/new", type: :view do
  before(:each) do
    assign(:comensal_hist_yumi, HistYumi.new())
  end

  it "renders new comensal_hist_yumi form" do
    render

    assert_select "form[action=?][method=?]", comensal_hist_yumis_path, "post" do
    end
  end
end
