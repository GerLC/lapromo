FactoryBot.define do
  factory :comment do
    kitchen_rating { 1 }
    ambient_rating { 1 }
    service_rating { 1 }
    description { Faker::Lorem.paragraphs }
    reservation
    invite_email { "" }
  end
end
